#!/usr/bin/env python2

import argparse
import errno
import logging
import os
import re

from tvnamer.utils import FileParser
from tvnamer.tvnamer_exceptions import (
    BaseTvnamerException, InvalidFilename, ShowNotFound,
)
from tvdb_api import Tvdb

VIDEO_EXTENSIONS = ['avi', 'mkv', 'mp4']
SERIES_NAME_MAP = {
    'Archer (2009)': 'Archer',
    'Castle (2009)': 'Castle',
    'House of Cards (US)': 'House of Cards',
    'The Office (US)': 'The Office',
}

logger = logging.getLogger('mvtv')


class MvtvError(BaseException):
    pass


def parse_args():
    parser = argparse.ArgumentParser(description='move tv episodes')
    parser.add_argument('--verbose', '-v', action='count')
    parser.add_argument('--silent', '-s', action='store_true')
    parser.add_argument('--dry-run', '-n', action='store_true')

    # Required optional arguments are usually a bad idea from a UI
    # perspective, but in this case it helps avoid a potentially destructive
    # operation. If dest was a positional argument, forgetting it while
    # passing mutiple source_dirs (coming soon) would lead to everything
    # being moved to the last source_dir.
    parser.add_argument('--dest', '-d', required=True)
    parser.add_argument('source_dir', metavar='SRC')

    raw_args = parser.parse_args()

    # some sanity checks
    if raw_args.verbose > 2:
        exit('You can\'t supply -v more than twice')
    if raw_args.verbose > 0 and raw_args.silent:
        exit('You can\'t supply both -s and -v')

    # convert args into something more meaningful
    args = argparse.Namespace()

    args.dry_run = raw_args.dry_run
    args.dest_dir = os.path.abspath(raw_args.dest)
    args.source_dir = os.path.abspath(raw_args.source_dir)

    if raw_args.silent:
        args.loglevel = logging.ERROR
    elif raw_args.verbose == 1:
        args.loglevel = logging.INFO
    elif raw_args.verbose == 2:
        args.loglevel = logging.DEBUG
    else:
        args.loglevel = logging.WARNING

    return args


def discover_videos(source_dir):
    """ discover all video files (except samples) inside source_dir """

    video_paths = []
    for directory_path, _, filenames in os.walk(source_dir):
        for filename in filenames:
            if '.' not in filename:
                continue

            if 'sample' in filename.lower():
                logger.warning('skipping (sample) %s' % filename)
                continue

            _, extension = filename.rsplit('.', 1)
            if extension in VIDEO_EXTENSIONS:
                video_paths.append(os.path.join(directory_path, filename))
    return video_paths


def filter_episodes(video_paths):
    """ convert and filter a list of videos into valid episodes """

    tvdb = Tvdb()
    episodes = []
    for video_path in video_paths:
        try:
            parser = FileParser(video_path)
            episode = parser.parse()
            episode.populateFromTvdb(tvdb)
        except (BaseTvnamerException, AttributeError) as e:

            if isinstance(e, InvalidFilename):
                reason = 'invalid'
            if isinstance(e, ShowNotFound):
                reason = 'not found'
            else:
                reason = 'parse'

            filename = os.path.basename(video_path)

            logger.warning('skipping (%s) %s: %s' % (reason, filename, e))
            continue

        logger.debug('found file %s' % episode)
        episodes.append(episode)
    return episodes


def dedupe_names(names):
    """ dedupe mutliple names of an episode

    If all names are the same after stripping any possible counters
    (eg "The Name (1)", "The Name (2)"), use it once. Otherwise, join all the
    names with commas.
    """
    pattern = re.compile(r'(.+?)(?:\s*\(\d+\))?$')
    stripped = [pattern.match(n).group(1) for n in names]

    # can't use a set because we need to preserve order
    if all(s == stripped[0] for s in stripped):
        # all the same
        name = stripped[0]
    else:
        # since they are not the same, assume that stripping the names is not
        # required and join the original names
        name = ', '.join(names)

    return name


def generate_title(episode):
    # fail if we don't have a season number
    if not hasattr(episode, 'seasonnumber'):
        raise MvtvError('missing season')

    # handle episode name
    if len(episode.episodename) > 1:
        name = dedupe_names(episode.episodename)
    else:
        name = episode.episodename[0]

    episodenumbers = ''.join('E%02d' % n for n in episode.episodenumbers)

    return 'S%02d%s - %s%s' % (
        episode.seasonnumber,
        episodenumbers,
        name,
        episode.extension
    )


def _escape_slashes(s):
    """ replace slashes (/) in s with colons (':')

    Usefull for creating files/directories. Since we need
    to escape the slash, we might as well use a colon since:

    - OS X Finder actually displays colons as slashes!
    - I'm going to remember why I did this for the above reason
    """
    return s.replace('/', ':')


def move_episode(episode, dest_dir, dry_run=False):
    """ move an episode to its predetermined destination """
    originalfilename = episode.originalfilename.decode('utf8')

    try:
        title = generate_title(episode).replace('/', ':')
    except MvtvError as e:
        logger.warning('skipping %s: %s' % (originalfilename, e.message))
        return

    seriesname = SERIES_NAME_MAP.get(episode.seriesname, episode.seriesname)
    directory = os.path.join(
        dest_dir,
        _escape_slashes(seriesname),
        'Season %d' % episode.seasonnumber,
    )
    path = os.path.join(
        directory,
        _escape_slashes(title),
    )


    if os.path.exists(path):
        m = 'already exists'
        logger.warning('skipping %s: %s' % (originalfilename, m))

    suffix = path[len(dest_dir):]
    logger.info('moving %s -> %s' % (originalfilename, suffix))
    if not dry_run:
        try:
            os.renames(episode.fullpath, path)
        except OSError as e:
            if not e.errno == errno.ENOTDIR:
                # can't handle it, let it propagate
                raise

            parent, _ = os.path.split(e.filename)
            m = '%s exists but is not a directory' % parent
            logger.warning('skipping %s: %s' % originalfilename, m)


if __name__ == '__main__':
    args = parse_args()
    logging.basicConfig(
        level=args.loglevel,
        format='%(levelname)s:%(message)s'
    )

    # disable tvdb_api logger
    tvlogger = logging.getLogger('tvdb_api')
    tvlogger.propagate = False
    tvlogger.addHandler(logging.NullHandler())

    video_paths = discover_videos(args.source_dir)
    episodes = filter_episodes(video_paths)

    for episode in episodes:
        move_episode(episode, args.dest_dir, args.dry_run)
